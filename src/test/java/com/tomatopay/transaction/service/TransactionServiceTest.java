package com.tomatopay.transaction.service;

import com.tomatopay.transaction.constant.TransactionConstant;
import com.tomatopay.transaction.mock.TransactionMockGenerator;
import com.tomatopay.transaction.model.Transaction;
import com.tomatopay.transaction.model.TransactionType;
import com.tomatopay.transaction.repository.TransactionRepository;
import com.tomatopay.transaction.service.impl.TransactionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @InjectMocks
    private TransactionServiceImpl transactionServiceImpl;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private AccountBalanceService accountBalanceService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetTransaction() {
        final Transaction transaction = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        final Optional<Transaction> transactionOptional = Optional.of(transaction);

        Mockito.when(transactionRepository.findById(any(UUID.class))).thenReturn(transactionOptional);

        final Transaction expectedResponse = transactionServiceImpl
            .getTransaction(UUID.fromString(TransactionConstant.TRANSACTION_ID));

        Assertions.assertEquals(transaction.getId(), expectedResponse.getId());
    }

    @Test
    public void testDeleteTransaction() {
        final Transaction transaction = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        final Optional<Transaction> transactionOptional = Optional.of(transaction);

        Mockito.when(transactionRepository.findById(any(UUID.class))).thenReturn(transactionOptional);
        Mockito.doNothing().when(accountBalanceService).updateAccountBalance(any(Transaction.class));

        final Transaction expectedResponse = transactionServiceImpl
            .deleteTransaction(UUID.fromString(TransactionConstant.TRANSACTION_ID));

        Assertions.assertEquals(transaction.getId(), expectedResponse.getId());
    }

    @Test
    public void testUpdateTransaction() {
        final Transaction transaction = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        final Optional<Transaction> transactionOptional = Optional.of(transaction);

        Mockito.when(transactionRepository.findById(any(UUID.class))).thenReturn(transactionOptional);
        Mockito.doNothing().when(accountBalanceService).createAccountBalance(any(Transaction.class));
        Mockito.doNothing().when(accountBalanceService).updateAccountBalance(any(Transaction.class));

        final Transaction expectedResponse = transactionServiceImpl.updateOrCreateTransaction(transaction);

        Assertions.assertEquals(transaction.getId(), expectedResponse.getId());
    }

    @Test
    public void testGetTransactions() {
        final List<Transaction> expectedResponse = new ArrayList<>();
        final Transaction transaction1 = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        final Transaction transaction2 = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID_2,
            TransactionConstant.CURRENCY_2,
            TransactionConstant.AMOUNT_2,
            TransactionConstant.DESCRIPTION_2,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        expectedResponse.add(transaction1);
        expectedResponse.add(transaction2);

        final Page<Transaction> transactionPage = new PageImpl<>(expectedResponse);

        Mockito
            .when(transactionRepository.findAll(PageRequest.of(0, 2)))
            .thenReturn(transactionPage);

        final List<Transaction> serviceResponse = transactionServiceImpl.getTransactions(0,2);

        Assertions.assertEquals(serviceResponse, expectedResponse);
    }

    @Test
    public void testCreateTransactions() {
        final List<Transaction> expectedResponse = new ArrayList<>();
        final Transaction transaction1 = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        final Transaction transaction2 = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID_2,
            TransactionConstant.CURRENCY_2,
            TransactionConstant.AMOUNT_2,
            TransactionConstant.DESCRIPTION_2,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        expectedResponse.add(transaction1);
        expectedResponse.add(transaction2);

        Mockito.when(transactionRepository.saveAll(any(List.class))).thenReturn(expectedResponse);
        Mockito.doNothing().when(accountBalanceService).createAccountBalances(anyList());

        final List<UUID> serviceResponse = transactionServiceImpl.createTransactions(expectedResponse);

        Assertions.assertEquals(serviceResponse, expectedResponse.stream().map(Transaction::getId).collect(Collectors.toList()));
    }
}
