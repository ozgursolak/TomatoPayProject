package com.tomatopay.transaction.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.tomatopay.transaction.constant.TestEndpoint;
import com.tomatopay.transaction.constant.TransactionConstant;
import com.tomatopay.transaction.mock.TransactionMockGenerator;
import com.tomatopay.transaction.model.Transaction;
import com.tomatopay.transaction.model.TransactionType;
import com.tomatopay.transaction.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;

public class TransactionControllerTest extends AbstractControllerTestBase {

    @MockBean
    private TransactionService transactionService;

    @Test
    public void whenGetTransaction_thenResponseIsOk() throws Exception {
        final Transaction serviceResponse = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        when(transactionService.getTransaction(any(UUID.class))).thenReturn(serviceResponse);

        final String responseJson = getMvc().perform(get(TestEndpoint.TRANSACTION))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        final Transaction response = getMapper().readValue(responseJson, Transaction.class);

        assertEquals(response.getAccountId(), serviceResponse.getAccountId());
        assertEquals(response.getCurrency(), serviceResponse.getCurrency());
        assertEquals(response.getDescription(), serviceResponse.getDescription());
        assertEquals(response.getType(), serviceResponse.getType());
    }

    @Test
    public void whenDeleteTransaction_thenResponseIsOk() throws Exception {
        final Transaction serviceResponse = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        when(transactionService.deleteTransaction(any(UUID.class))).thenReturn(serviceResponse);

        final String responseJson = getMvc().perform(delete(TestEndpoint.TRANSACTION))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        final Transaction response = getMapper().readValue(responseJson, Transaction.class);

        assertEquals(response.getAccountId(), serviceResponse.getAccountId());
        assertEquals(response.getCurrency(), serviceResponse.getCurrency());
        assertEquals(response.getDescription(), serviceResponse.getDescription());
        assertEquals(response.getType(), serviceResponse.getType());
    }

    @Test
    public void whenPutTransaction_thenResponseIsOk() throws Exception {
        final Transaction serviceResponse = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        when(transactionService.updateOrCreateTransaction(any())).thenReturn(serviceResponse);

        final String responseJson = getMvc()
                .perform(put(TestEndpoint.TRANSACTION)
                    .content(asJsonString(serviceResponse))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        final Transaction response = getMapper().readValue(responseJson, Transaction.class);

        assertEquals(response.getAccountId(), serviceResponse.getAccountId());
        assertEquals(response.getCurrency(), serviceResponse.getCurrency());
        assertEquals(response.getDescription(), serviceResponse.getDescription());
        assertEquals(response.getType(), serviceResponse.getType());
    }

    @Test
    public void whenGetTransactions_thenResponseIsOk() throws Exception {
        final List<Transaction> serviceResponse = new ArrayList<>();
        final Transaction transaction1 = TransactionMockGenerator.createTransaction(
                TransactionConstant.TRANSACTION_ID,
                TransactionConstant.ACCOUNT_ID,
                TransactionConstant.CURRENCY,
                TransactionConstant.AMOUNT,
                TransactionConstant.DESCRIPTION,
                TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        final Transaction transaction2 = TransactionMockGenerator.createTransaction(
                TransactionConstant.TRANSACTION_ID_2,
                TransactionConstant.ACCOUNT_ID_2,
                TransactionConstant.CURRENCY_2,
                TransactionConstant.AMOUNT_2,
                TransactionConstant.DESCRIPTION_2,
                TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        serviceResponse.add(transaction1);
        serviceResponse.add(transaction2);

        when(transactionService.getTransactions(anyInt(), anyInt())).thenReturn(serviceResponse);

        final LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();

        requestParams.add("pageNumber", "0");
        requestParams.add("pageSize", "2");

        final String responseJson = getMvc()
                .perform(get(TestEndpoint.TRANSACTIONS)
                        .params(requestParams)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        final List<Transaction> response = getMapper().readValue(responseJson, List.class);

        assertEquals(response.size(), serviceResponse.size());
    }

    @Test
    public void whenPostTransactions_thenResponseIsOk() throws Exception {
        final List<UUID> serviceResponse = new ArrayList<>();
        final List<Transaction> transactions = new ArrayList<>();
        final Transaction transaction1 = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID,
            TransactionConstant.ACCOUNT_ID,
            TransactionConstant.CURRENCY,
            TransactionConstant.AMOUNT,
            TransactionConstant.DESCRIPTION,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        final Transaction transaction2 = TransactionMockGenerator.createTransaction(
            TransactionConstant.TRANSACTION_ID_2,
            TransactionConstant.ACCOUNT_ID_2,
            TransactionConstant.CURRENCY_2,
            TransactionConstant.AMOUNT_2,
            TransactionConstant.DESCRIPTION_2,
            TransactionType.valueOf(TransactionConstant.TRANSACTION_TYPE)
        );

        serviceResponse.add(transaction1.getId());
        serviceResponse.add(transaction2.getId());

        transactions.add(transaction1);
        transactions.add(transaction2);

        when(transactionService.createTransactions(anyList())).thenReturn(serviceResponse);

        final String responseJson = getMvc()
                .perform(post(TestEndpoint.TRANSACTIONS)
                    .content(asJsonString(transactions))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        final List<String> response = getMapper().readValue(responseJson, List.class);
        final List<UUID> responseAsUUID = response.stream().map(id -> UUID.fromString(id)).collect(Collectors.toList());

        assertEquals(responseAsUUID, serviceResponse);
    }
}
