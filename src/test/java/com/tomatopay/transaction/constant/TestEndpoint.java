package com.tomatopay.transaction.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TestEndpoint {
    public static final String TRANSACTION = "/transaction/4B36AFC8-5205-49C1-AF16-4DC6F96DB132";
    public static final String TRANSACTIONS = "/transactions";
}
