package com.tomatopay.transaction.constant;

public class TransactionConstant {

    public static final String TRANSACTION_ID ="4b36afc8-5205-49c1-af16-4dc6f96db130";
    public static final String ACCOUNT_ID ="4b36afc8-5205-49c1-af16-4dc6f96db132";
    public static final String DESCRIPTION = "Tomato";
    public static final String CURRENCY = "GBP";
    public static final String TRANSACTION_TYPE = "CREDIT";
    public static final Double AMOUNT = new Double(20);

    public static final String TRANSACTION_ID_2 ="4b36afc8-5205-49c1-af16-4dc6f96db131";
    public static final String ACCOUNT_ID_2 ="4b36afc8-5205-49c1-af16-4dc6f96db133";
    public static final String DESCRIPTION_2 = "Potato";
    public static final String CURRENCY_2 = "GBP";
    public static final Double AMOUNT_2 = new Double(40);
}
