package com.tomatopay.transaction.mock;

import com.tomatopay.transaction.model.Transaction;
import com.tomatopay.transaction.model.TransactionType;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class TransactionMockGenerator {

    public static Transaction createTransaction(final String id, final String accountId,
        final  String currency, final Double amount, final String description, final TransactionType transactionType) {

        final Transaction transaction = new Transaction();

        transaction.setId(UUID.fromString(id));
        transaction.setAccountId(UUID.fromString(accountId));
        transaction.setType(transactionType);
        transaction.setAmount(amount);
        transaction.setDescription(currency);
        transaction.setDescription(description);

        return transaction;
    }
}
