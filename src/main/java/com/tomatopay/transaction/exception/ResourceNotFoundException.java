package com.tomatopay.transaction.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.function.Supplier;

@Data
public class ResourceNotFoundException  extends RuntimeException {

    private final HttpStatus httpStatus;

    public ResourceNotFoundException(final HttpStatus httpStatus, final String message) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
