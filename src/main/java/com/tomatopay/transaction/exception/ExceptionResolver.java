package com.tomatopay.transaction.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class ExceptionResolver extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> handleBaseException(final ResourceNotFoundException exception) {
        final ExceptionDetails exceptionDetails = new ExceptionDetails();
        final HttpHeaders headers = new HttpHeaders();

        exceptionDetails.setMessage(exception.getMessage());

        headers.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(exceptionDetails, headers, exception.getHttpStatus());
    }
}