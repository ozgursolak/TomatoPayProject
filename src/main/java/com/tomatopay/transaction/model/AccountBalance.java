package com.tomatopay.transaction.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "ACCOUNT_BALANCE")
@Where(clause = "is_deleted = false")
public class AccountBalance extends AbstractDateEntity {

    @Column(name = "accountId", unique = true, nullable = false)
    private UUID accountId;

    @Column(name = "balance")
    private Double balance = new Double(0);
}
