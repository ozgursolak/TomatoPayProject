package com.tomatopay.transaction.model;

import javax.persistence.*;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Where;

@Getter
@Setter
@Entity
@Table(name = "TRANSACTIONS")
@Where(clause = "is_deleted = false")
public class Transaction extends AbstractDateEntity {

    @Column(name = "accountId")
    @JsonProperty("accountId")
    private UUID accountId;

    @Column(name = "currency", columnDefinition = "varchar(10)")
    @JsonProperty("currency")
    private String currency;

    @Column(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    @Column(name = "description")
    @JsonProperty("description")
    private String description;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    @JsonProperty("type")
    private TransactionType type;
}
