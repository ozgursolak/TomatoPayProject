package com.tomatopay.transaction.model;

import lombok.Getter;

@Getter
public enum TransactionType {

    DEBIT,
    CREDIT
}
