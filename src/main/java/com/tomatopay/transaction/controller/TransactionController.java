package com.tomatopay.transaction.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.tomatopay.transaction.constant.Endpoint;
import com.tomatopay.transaction.model.Transaction;
import com.tomatopay.transaction.service.TransactionService;

import lombok.RequiredArgsConstructor;

@CrossOrigin(origins = "localhost")
@RestController
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;

    @GetMapping(Endpoint.TRANSACTION)
    public ResponseEntity<Transaction> getTransaction(@PathVariable final UUID transactionId) {
        final Transaction transaction = transactionService.getTransaction(transactionId);

        return ResponseEntity.ok(transaction);
    }

    @PutMapping(Endpoint.TRANSACTION)
    public ResponseEntity<Transaction> updateTransaction(@RequestBody final Transaction transaction) {
        final Transaction updatedTransaction = transactionService.updateOrCreateTransaction(transaction);

        return ResponseEntity.ok(updatedTransaction);
    }

    @DeleteMapping(Endpoint.TRANSACTION)
    public ResponseEntity<Transaction> deleteTransaction(@PathVariable final UUID transactionId) {
        final Transaction transaction = transactionService.deleteTransaction(transactionId);

        return ResponseEntity.ok(transaction);
    }

    @GetMapping(Endpoint.TRANSACTIONS)
    public ResponseEntity<List<Transaction>> getTransactions(@RequestParam final int pageNumber,
        @RequestParam final int pageSize) {

        final List<Transaction> transactions = transactionService.getTransactions(pageNumber, pageSize);

        return ResponseEntity.ok(transactions);
    }

    @PostMapping(Endpoint.TRANSACTIONS)
    public ResponseEntity<List<UUID>> createTransactions(@RequestBody final List<Transaction> transactions) {
        final List<UUID> transactionIds = transactionService.createTransactions(transactions);

        return ResponseEntity.ok(transactionIds);
    }
}
