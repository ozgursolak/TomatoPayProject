package com.tomatopay.transaction.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Endpoint {

    public final String TRANSACTIONS = "/transactions";
    public final String TRANSACTION = "/transaction/{transactionId}";
}

