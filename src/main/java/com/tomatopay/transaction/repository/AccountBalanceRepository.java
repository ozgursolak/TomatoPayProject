package com.tomatopay.transaction.repository;

import com.tomatopay.transaction.model.AccountBalance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AccountBalanceRepository extends JpaRepository<AccountBalance, UUID> {

    AccountBalance findByAccountId(UUID accountId);
}
