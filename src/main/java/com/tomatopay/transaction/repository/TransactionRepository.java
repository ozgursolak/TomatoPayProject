package com.tomatopay.transaction.repository;

import java.util.UUID;

import com.tomatopay.transaction.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, UUID> {
}
