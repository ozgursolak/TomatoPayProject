package com.tomatopay.transaction.service.impl;

import com.tomatopay.transaction.model.AccountBalance;
import com.tomatopay.transaction.model.Transaction;
import com.tomatopay.transaction.model.TransactionType;
import com.tomatopay.transaction.repository.AccountBalanceRepository;
import com.tomatopay.transaction.service.AccountBalanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AccountBalanceServiceImpl implements AccountBalanceService {

    private final AccountBalanceRepository accountBalanceRepository;

    @Override
    @Async("asyncExecutor")
    public void updateAccountBalance(final Transaction transaction) {
        final AccountBalance accountBalance = accountBalanceRepository.findByAccountId(transaction.getAccountId());

        if (!Objects.isNull(accountBalance)) {
            if (transaction.getType() == TransactionType.CREDIT) {
                accountBalance.setBalance(accountBalance.getBalance() - transaction.getAmount());
            } else {
                accountBalance.setBalance(accountBalance.getBalance() + transaction.getAmount());
            }

            accountBalanceRepository.save(accountBalance);
        }
    }

    @Override
    @Async("asyncExecutor")
    public void updateAccountBalanceForDelete(final Transaction transaction) {
        final AccountBalance accountBalance = accountBalanceRepository.findByAccountId(transaction.getAccountId());

        if (!Objects.isNull(accountBalance)) {
            if (transaction.getType() == TransactionType.CREDIT) {
                accountBalance.setBalance(accountBalance.getBalance() + transaction.getAmount());
            } else {
                accountBalance.setBalance(accountBalance.getBalance() - transaction.getAmount());
            }

            accountBalanceRepository.save(accountBalance);
        }
    }

    @Override
    @Async("asyncExecutor")
    public void createAccountBalance(final Transaction transaction) {
        AccountBalance accountBalance = accountBalanceRepository.findByAccountId(transaction.getAccountId());

        if (Objects.isNull(accountBalance)) {
            accountBalance = new AccountBalance();
            accountBalance.setAccountId(transaction.getAccountId());
        }

        if (transaction.getType() == TransactionType.CREDIT) {
            accountBalance.setBalance(accountBalance.getBalance() - transaction.getAmount());
        } else {
            accountBalance.setBalance(accountBalance.getBalance() + transaction.getAmount());
        }

        accountBalanceRepository.save(accountBalance);
    }

    @Override
    @Async("asyncExecutor")
    public void createAccountBalances(final List<Transaction> transactions) {
        transactions.forEach(this::createAccountBalance);
    }
}
