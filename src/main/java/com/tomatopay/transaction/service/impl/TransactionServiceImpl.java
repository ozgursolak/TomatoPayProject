package com.tomatopay.transaction.service.impl;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.tomatopay.transaction.exception.ResourceNotFoundException;
import com.tomatopay.transaction.model.Transaction;
import com.tomatopay.transaction.repository.TransactionRepository;
import com.tomatopay.transaction.service.AccountBalanceService;
import com.tomatopay.transaction.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountBalanceService accountBalanceService;

    @Override
    @Transactional
    public Transaction getTransaction(final UUID transactionId) {
        final Optional<Transaction> transactionOptional = transactionRepository.findById(transactionId);

        return transactionOptional.orElseThrow(() ->
            new ResourceNotFoundException(
                HttpStatus.NOT_FOUND,
                "Transaction with id: " + transactionId + " can not be found"
            )
        );
    }

    @Override
    @Transactional
    public Transaction updateOrCreateTransaction(final Transaction transaction) {
        final Optional<Transaction> transactionOptional = transactionRepository.findById(transaction.getId());

        if (transactionOptional.isPresent()) {
            final Transaction oldTransaction = transactionOptional.get();

            oldTransaction.setAccountId(transaction.getAccountId());
            oldTransaction.setAmount(transaction.getAmount());
            oldTransaction.setCurrency(transaction.getCurrency());
            oldTransaction.setDescription(transaction.getDescription());
            oldTransaction.setType(transaction.getType());

            accountBalanceService.updateAccountBalance(oldTransaction);

            return oldTransaction;
        }

        accountBalanceService.createAccountBalance(transaction);

        return transactionRepository.save(transaction);
    }

    @Override
    @Transactional
    public Transaction deleteTransaction(final UUID transactionId) {
        final Optional<Transaction> transactionOptional = transactionRepository.findById(transactionId);
        final Transaction transaction = transactionOptional.orElseThrow(() ->
                new ResourceNotFoundException(
                        HttpStatus.NOT_FOUND,
                        "Transaction with id: " + transactionId + " can not be found"
                )
        );

        transaction.setIsDeleted(Boolean.TRUE);

        accountBalanceService.updateAccountBalanceForDelete(transaction);

        transactionRepository.save(transaction);

        return transaction;
    }

    @Override
    @Transactional
    public List<Transaction> getTransactions(final int pageNumber, final int pageSize) {
        return transactionRepository.findAll(PageRequest.of(pageNumber, pageSize)).getContent();
    }

    @Override
    @Transactional
    public List<UUID> createTransactions(final List<Transaction> transactions) {
        final List<Transaction> newTransactions = transactionRepository.saveAll(transactions);

        accountBalanceService.createAccountBalances(newTransactions);

        return newTransactions
            .stream()
            .map(Transaction::getId)
            .collect(Collectors.toList());
    }
}
