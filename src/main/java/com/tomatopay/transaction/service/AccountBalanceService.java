package com.tomatopay.transaction.service;

import com.tomatopay.transaction.model.Transaction;

import java.util.List;

public interface AccountBalanceService {

    void updateAccountBalance(Transaction transaction);
    void updateAccountBalanceForDelete(Transaction transaction);
    void createAccountBalance(Transaction transaction);
    void createAccountBalances(List<Transaction> transactions);
}
