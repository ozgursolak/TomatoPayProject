package com.tomatopay.transaction.service;

import java.util.List;
import java.util.UUID;

import com.tomatopay.transaction.model.Transaction;

public interface TransactionService {

    Transaction getTransaction(UUID transactionId);
    Transaction updateOrCreateTransaction(Transaction transaction);
    Transaction deleteTransaction(UUID transactionId);
    List<Transaction> getTransactions(int pageNumber, int pageSize);
    List<UUID> createTransactions(List<Transaction> transactions);
}
