# Tomatopay Transaction Microservice

This microservice is responsible for creating, deleting and updating transactions.
## Important For Setup:

* The application uses default spring boot port (8080).<br />
* For datastore, this application uses **Postgresql**.<br />
  To setup  a postgre db server on port 5433, you can use this script:<br />
```
  docker run -d -v tomato-pay-volume:/var/lib/postgresql/tomatopay/data  --name tomatopay_local_db -e POSTGRES_HOST_AUTH_METHOD=trust -p 5433:5432 -i postgres
```

* After creation of server, you can create  a database which is named as **tomatopay_local_db**.<br />
* If you want to change the configuration, you can do it on **application.properties**.

* After starting the application, the app will create two different tables on database.
  One of them, which is named as **transactions**, will store the transactions that
  are created via post request to **transactions** endpoint.

* Another table, which is named as **account_balance**, will responsible for storing
  the balance of each account which is already created.

* Note that, I assumed that debit transactions increase the balance, on the other hand, credit transactions  decrease the balance. <br />
  Also, I assumed that all transactions for an account are being made with **the same currency**.

* After booting the app, you can send a post request to **/transactions** endpoint in order to create transactions in the db.<br />

* Example post request to **/transactions** endpoint: <br />
``` json
curl --location --request POST 'localhost:8080/transactions' \
--header 'Content-Type: application/json' \
--data-raw '[
    {
    "accountId": "123e4567-e89b-42d3-a456-556642440002",
    "currency": "GBP",
    "amount": 100,
    "description": "Tesco Holborn Station",
    "type": "CREDIT"
    },
    {
    "accountId": "123e4567-e89b-42d3-a456-556642440003",
    "currency": "GBP",
    "amount": 100,
    "description": "Tesco2",
    "type": "CREDIT"
    },
    {
    "accountId": "123e4567-e89b-42d3-a456-556642440004",
    "currency": "GBP",
    "amount": 400,
    "description": "Tesco3",
    "type": "DEBIT"
    }
]'
```

* Example get request to **/transactions** endpoint to get list of transactions:
  Note that, I used pagination in this endpoint. Because of that, pageSize and pageNumber are used as parameter.<br />
```
  curl --location --request GET 'localhost:8080/transactions?pageNumber=0&pageSize=1'
```

* Example get request to **/transaction/7660714c-9139-42f0-a1d8-9e2f4abee257** endpoint to get a specific transaction with id **7660714c-9139-42f0-a1d8-9e2f4abee257** : <br />
```  
curl --location --request GET 'localhost:8080/transaction/7660714c-9139-42f0-a1d8-9e2f4abee257'
```

* Example put request to **/transaction/6ef701b0-1189-49c0-acd5-cf30f4a7ce83** endpoint to update a specific transaction: <br />
```
curl --location --request PUT 'localhost:8080/transaction/6ef701b0-1189-49c0-acd5-cf30f4a7ce83' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": "6ef701b0-1189-49c0-acd5-cf30f4a7ce83",
    "accountId": "123e4567-e89b-42d3-a456-556642440002",
    "currency": "GBP",
    "amount": 800,
    "description": "Tesco Holborn Station",
    "type": "DEBIT"
}'
```

* Example delete request to **/transaction/d89ad994-2b6b-4841-83f8-91cdd8cf5706** endpoint to delete a specific transaction with id **d89ad994-2b6b-4841-83f8-91cdd8cf5706**: <br />
```
curl --location --request DELETE 'localhost:8080/transaction/d89ad994-2b6b-4841-83f8-91cdd8cf5706'
```
